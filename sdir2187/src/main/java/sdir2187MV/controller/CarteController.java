package sdir2187MV.controller;

import sdir2187MV.model.Carte;
import sdir2187MV.repository.CartiRepoInterface;
import sdir2187MV.repository.CartiRepoMock;
import sdir2187MV.util.Validator;

import java.util.List;

public class CarteController {

	private CartiRepoInterface cr;
	private CartiRepoMock crm;
	private Validator validator;
	public CarteController(CartiRepoInterface cr){
		this.cr = cr;
	}

	
	public void adaugaCarte(Carte c) throws Exception{
		Validator.validateCarte(c);
		cr.adaugaCarte(c);
	}
	
	public void modificaCarte(Carte nou, Carte vechi) throws Exception{
		cr.modificaCarte(nou, vechi);
	}
	
	public void stergeCarte(Carte c) throws Exception{
		cr.stergeCarte(c);
	}

	public List<Carte> cautaCarte(String autor) throws Exception{
		Validator.isStringOK(autor);
		return cr.cautaCarte(autor);
	}
	
	public List<Carte> getCarti() throws Exception{
		return cr.getCarti();
	}
	
	public List<Carte> getCartiOrdonateDinAnul(int an) throws Exception{
		if(!Validator.isNumber(String.valueOf(an)))
			throw new Exception("Nu e numar!");
		return cr.getCartiOrdonateDinAnul(an);
	}
	
	
}
