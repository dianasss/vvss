package sdir2187MV.repository;


import sdir2187MV.model.Carte;
import java.util.List;

public interface CartiRepoInterface {
	void adaugaCarte(Carte c);
	void modificaCarte(Carte nou, Carte vechi);
	void stergeCarte(Carte c);
	List<Carte> cautaCarte(String ref) throws Exception;
	List<Carte> getCarti();
	List<Carte> getCartiOrdonateDinAnul(int an);
}
