package sdir2187MV.repository;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import sdir2187MV.model.Carte;

import java.util.List;

import static org.junit.Assert.*;


public class CartiRepoMockTest {

    private CartiRepoMock cartiRepoMock;
    @Before
    public void setUp() throws Exception {
        cartiRepoMock=new CartiRepoMock();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void cautaCarte_nonvalid() throws Exception {

        cartiRepoMock.adaugaCarte(Carte.getCarteFromString("Povesti;Mihai Eminescu,Ion Caragiale,Ion Creanga;1973;Corint;povesti,povestiri"));
        try {
            cartiRepoMock.cautaCarte("1234");
            assertFalse(true);
        }
        catch (Exception e){
            assertFalse (false);
        }
    }

    @Test
    public void cautaCarte_valid_path1() throws Exception {

        //0 it
        try {
            cartiRepoMock.cautaCarte("ion");
            assertTrue(true);
        }
        catch (Exception e){
            assertTrue (false);
        }
    }

    @Test
    public void cautaCarte_valid_path2() throws Exception {

        //2 it.
        cartiRepoMock.adaugaCarte(new Carte());
        cartiRepoMock.adaugaCarte(new Carte());
        List<Carte> c=cartiRepoMock.getCarti();
        System.out.println(c);
        System.out.println(c.get(0).getReferenti().size());
        System.out.println();
        try {
            cartiRepoMock.cautaCarte("ion");
            assertTrue(true);
        }
        catch (Exception e){
            assertTrue (false);
        }
    }

    @Test
    public void cautaCarte_valid_path4() throws Exception {

        //4 it.
        cartiRepoMock.adaugaCarte(Carte.getCarteFromString("Povesti;Mihai Eminescu,Ion Caragiale,Ion Creanga;1973;Corint;povesti,povestiri"));
        cartiRepoMock.adaugaCarte(Carte.getCarteFromString("Poezii;ion Sadoveanu;1973;Corint;poezii"));
        cartiRepoMock.adaugaCarte(Carte.getCarteFromString("Dale carnavalului;Caragiale Ion;1948;Litera;caragiale,carnaval"));
        cartiRepoMock.adaugaCarte(Carte.getCarteFromString("Poezii;ion Sadoveanu;1973;Corint;poezii"));
        try {
            cartiRepoMock.cautaCarte("ion");
            assertTrue(true);
        }
        catch (Exception e){
            assertTrue (false);
        }
    }

    @Test
    public void cautaCarte_valid_path5() throws Exception {

        //1 it.
        cartiRepoMock.adaugaCarte(Carte.getCarteFromString("Povesti;Mihai Eminescu,Ion Caragiale,Ion Creanga;1973;Corint;povesti,povestiri"));
        try {
            cartiRepoMock.cautaCarte("x");
            assertTrue(true);
        }
        catch (Exception e){
            assertTrue (false);
        }
    }

    @Test
    public void cautaCarteDinAnul_exista() throws Exception {
        cartiRepoMock.adaugaCarte(Carte.getCarteFromString("Dale carnavalului;Caragiale Ion;1948;Litera;caragiale,carnaval"));
        cartiRepoMock.adaugaCarte(Carte.getCarteFromString("Povesti;Mihai Eminescu,Ion Caragiale,Ion Creanga;1973;Corint;povesti,povestiri"));
        assertEquals(cartiRepoMock.getCartiOrdonateDinAnul(1973).size(),1);
    }

    @Test
    public void cautaCarteDinAnul_nuexista() throws Exception {
        cartiRepoMock.adaugaCarte(Carte.getCarteFromString("Dale carnavalului;Caragiale Ion;1948;Litera;caragiale,carnaval"));
        cartiRepoMock.adaugaCarte(Carte.getCarteFromString("Povesti;Mihai Eminescu,Ion Caragiale,Ion Creanga;1973;Corint;povesti,povestiri"));
        assertEquals(cartiRepoMock.getCartiOrdonateDinAnul(1971).size(),0);
    }

}