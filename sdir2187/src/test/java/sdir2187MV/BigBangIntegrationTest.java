package sdir2187MV;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import sdir2187MV.controller.CarteController;
import sdir2187MV.model.Carte;
import sdir2187MV.repository.CartiRepo;
import sdir2187MV.repository.CartiRepoMock;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

public class BigBangIntegrationTest {

    private CartiRepoMock cartiRepoMock;

    @Before
    public void setUp() throws Exception {
        cartiRepoMock=new CartiRepoMock();
    }

    @Test
    public void adaugaCarte() throws Exception {
        cartiRepoMock.adaugaCarte(Carte.getCarteFromString("Povesti;Mihai Eminescu,Ion Caragiale,Ion Creanga;1973;Corint;povesti,povestiri"));
        assertEquals(cartiRepoMock.getCarti().size(), 1);
    }

    @Test
    public void cautaCarte() throws Exception {

        cartiRepoMock.adaugaCarte(Carte.getCarteFromString("Povesti;Mihai Eminescu,Ion Caragiale,Ion Creanga;1973;Corint;povesti,povestiri"));
        cartiRepoMock.adaugaCarte(Carte.getCarteFromString("Poezii;ion Sadoveanu;1973;Corint;poezii"));
        cartiRepoMock.adaugaCarte(Carte.getCarteFromString("Dale carnavalului;Caragiale Ion;1948;Litera;caragiale,carnaval"));
        cartiRepoMock.adaugaCarte(Carte.getCarteFromString("Poezii;ion Sadoveanu;1973;Corint;poezii"));
        assertEquals(cartiRepoMock.cautaCarte("ion").size(),4);
    }

    public void cautaCarteDinAnul() throws Exception {
        cartiRepoMock.adaugaCarte(Carte.getCarteFromString("Dale carnavalului;Caragiale Ion;1948;Litera;caragiale,carnaval"));
        cartiRepoMock.adaugaCarte(Carte.getCarteFromString("Povesti;Mihai Eminescu,Ion Caragiale,Ion Creanga;1973;Corint;povesti,povestiri"));
        assertEquals(cartiRepoMock.getCartiOrdonateDinAnul(1973).size(),1);
    }

    @Test
    public void integrationTest() {
        try {
            cartiRepoMock.adaugaCarte(Carte.getCarteFromString("Povesti;Mihai Eminescu,Ion Caragiale,Ion Creanga;1973;Corint;povesti,povestiri"));
            cartiRepoMock.adaugaCarte(Carte.getCarteFromString("Dale carnavalului;Caragiale Ion;1948;Litera;caragiale,carnaval"));

            Assert.assertEquals(cartiRepoMock.getCarti().size(),2);

            Assert.assertEquals(cartiRepoMock.cautaCarte("Mihai").size(), 1);
            Assert.assertEquals(cartiRepoMock.cautaCarte("Ion").size(), 2);

            Assert.assertEquals(cartiRepoMock.getCartiOrdonateDinAnul(1973).size(), 1);
            Assert.assertEquals(cartiRepoMock.getCartiOrdonateDinAnul(1200).size(), 0);
        } catch (Exception e) {
            fail();
        }
    }




}
