package sdir2187MV;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import sdir2187MV.model.Carte;
import sdir2187MV.repository.CartiRepoMock;

import static org.junit.Assert.assertEquals;

public class TopDownIntegrationTest {

    private CartiRepoMock cartiRepoMock;

    @Before
    public void setUp() throws Exception {
        cartiRepoMock=new CartiRepoMock();
    }

    @Test
    public void adaugaCarte() throws Exception {
        cartiRepoMock.adaugaCarte(Carte.getCarteFromString("Povesti;Mihai Eminescu,Ion Caragiale,Ion Creanga;1973;Corint;povesti,povestiri"));
        assertEquals(cartiRepoMock.getCarti().size(), 1);
    }

    @Test
    public void cautaCarte() throws Exception {

        cartiRepoMock.adaugaCarte(Carte.getCarteFromString("Povesti;Mihai Eminescu,Ion Caragiale,Ion Creanga;1973;Corint;povesti,povestiri"));
        cartiRepoMock.adaugaCarte(Carte.getCarteFromString("Poezii;ion Sadoveanu;1973;Corint;poezii"));
        cartiRepoMock.adaugaCarte(Carte.getCarteFromString("Dale carnavalului;Caragiale Ion;1948;Litera;caragiale,carnaval"));
        cartiRepoMock.adaugaCarte(Carte.getCarteFromString("Poezii;ion Sadoveanu;1973;Corint;poezii"));
        assertEquals(cartiRepoMock.cautaCarte("ion").size(),4);
    }

    @Test
    public void cautaCarteDinAnul() throws Exception {
        cartiRepoMock.adaugaCarte(Carte.getCarteFromString("Dale carnavalului;Caragiale Ion;1948;Litera;caragiale,carnaval"));
        cartiRepoMock.adaugaCarte(Carte.getCarteFromString("Povesti;Mihai Eminescu,Ion Caragiale,Ion Creanga;1973;Corint;povesti,povestiri"));
        assertEquals(cartiRepoMock.getCartiOrdonateDinAnul(1973).size(),1);
        assertEquals(cartiRepoMock.getCartiOrdonateDinAnul(1200).size(),0);
    }


    @Test
    public void integrationPA() throws Exception {
        try {
            cartiRepoMock.adaugaCarte(Carte.getCarteFromString("Povesti;Mihai Eminescu,Ion Caragiale,Ion Creanga;1973;Corint;povesti,povestiri"));
            Assert.assertEquals(cartiRepoMock.getCarti().size(), 1);

        } finally {
            cartiRepoMock.getCarti().remove(Carte.getCarteFromString("Povesti;Mihai Eminescu,Ion Caragiale,Ion Creanga;1973;Corint;povesti,povestiri"));
        }
    }

    @Test
    public void integrationPAB() throws Exception {
        try {
            cartiRepoMock.adaugaCarte(Carte.getCarteFromString("Povesti;Mihai Eminescu,Ion Caragiale,Ion Creanga;1973;Corint;povesti,povestiri"));
            Assert.assertEquals(cartiRepoMock.getCarti().size(), 1);
            cartiRepoMock.adaugaCarte(Carte.getCarteFromString("Poezii;ion Sadoveanu;1973;Corint;poezii"));
            cartiRepoMock.adaugaCarte(Carte.getCarteFromString("Dale carnavalului;Caragiale Ion;1948;Litera;caragiale,carnaval"));
            cartiRepoMock.adaugaCarte(Carte.getCarteFromString("Poezii;ion Sadoveanu;1973;Corint;poezii"));
            assertEquals(cartiRepoMock.cautaCarte("ion").size(),4);

        } finally {
            cartiRepoMock.getCarti().remove(Carte.getCarteFromString("Poezii;ion Sadoveanu;1973;Corint;poezii"));
            cartiRepoMock.getCarti().remove(Carte.getCarteFromString("Poezii;ion Sadoveanu;1973;Corint;poezii"));
            cartiRepoMock.getCarti().remove(Carte.getCarteFromString("Dale carnavalului;Caragiale Ion;1948;Litera;caragiale,carnaval"));
            cartiRepoMock.getCarti().remove(Carte.getCarteFromString("Povesti;Mihai Eminescu,Ion Caragiale,Ion Creanga;1973;Corint;povesti,povestiri"));
        }
    }

    @Test
    public void integrationPABC() throws Exception {
        try {
            cartiRepoMock.adaugaCarte(Carte.getCarteFromString("Povesti;Mihai Eminescu,Ion Caragiale,Ion Creanga;1973;Corint;povesti,povestiri"));
            Assert.assertEquals(cartiRepoMock.getCarti().size(), 1);
            cartiRepoMock.adaugaCarte(Carte.getCarteFromString("Poezii;ion Sadoveanu;1973;Corint;poezii"));
            cartiRepoMock.adaugaCarte(Carte.getCarteFromString("Dale carnavalului;Caragiale Ion;1948;Litera;caragiale,carnaval"));
            cartiRepoMock.adaugaCarte(Carte.getCarteFromString("Poezii;ion Sadoveanu;1973;Corint;poezii"));
            assertEquals(cartiRepoMock.cautaCarte("ion").size(),4);
            assertEquals(cartiRepoMock.getCartiOrdonateDinAnul(1948).size(),1);
            assertEquals(cartiRepoMock.getCartiOrdonateDinAnul(1200).size(),0);

        } finally {
            cartiRepoMock.getCarti().remove(Carte.getCarteFromString("Poezii;ion Sadoveanu;1973;Corint;poezii"));
            cartiRepoMock.getCarti().remove(Carte.getCarteFromString("Poezii;ion Sadoveanu;1973;Corint;poezii"));
            cartiRepoMock.getCarti().remove(Carte.getCarteFromString("Dale carnavalului;Caragiale Ion;1948;Litera;caragiale,carnaval"));
            cartiRepoMock.getCarti().remove(Carte.getCarteFromString("Povesti;Mihai Eminescu,Ion Caragiale,Ion Creanga;1973;Corint;povesti,povestiri"));
        }
    }

}
