package sdir2187MV.controller;

import org.junit.Before;
import org.junit.Test;
import sdir2187MV.model.Carte;
import sdir2187MV.repository.CartiRepoInterface;
import sdir2187MV.repository.CartiRepoMock;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class CarteControllerTest {

    CartiRepoInterface cr;
    CarteController bc;
    @Before
    public void setUp() throws Exception {
        cr= new CartiRepoMock();
        bc = new CarteController(cr);
    }

    @Test
    public void adaugaCarteECP_1_valid() throws Exception {
        Carte c1= new Carte();
        c1.setTitlu("Ion");
        List<String> referenti = new ArrayList<>();
        referenti.add("Liviu Rebreanu");
        c1.setReferenti(referenti);
        c1.setAnAparitie(1920);
        List<String> cheie = new ArrayList<>();
        cheie.add("roman");
        cheie.add("romanesc");
        c1.setCuvinteCheie(cheie);
        bc.adaugaCarte(c1);
        int car = bc.getCarti().size();
        assertTrue(car==7);
    }

    @Test(expected = java.lang.Exception.class)
    public void adaugaCarteECP_2_nonvalid() throws Exception {
        Carte c1= new Carte();
        c1.setTitlu("2Morometii");
        List<String> referenti = new ArrayList<>();
        referenti.add("Marin Preda");
        c1.setReferenti(referenti);
        c1.setAnAparitie(1955);
        List<String> cheie = new ArrayList<>();
        cheie.add("roman");
        cheie.add("interbelic");
        c1.setCuvinteCheie(cheie);
        bc.adaugaCarte(c1);
        int car = bc.getCarti().size();
        assertTrue(car==6);
    }

    @Test(expected = java.lang.Exception.class)
    public void adaugaCarteECP_4_nonvalid() throws Exception {
        Carte c1= new Carte();
        c1.setTitlu("Morometii");
        List<String> referenti = new ArrayList<>();
        referenti.add("Marin Preda");
        c1.setReferenti(referenti);
        c1.setAnAparitie(-2);
        List<String> cheie = new ArrayList<>();
        cheie.add("roman");
        cheie.add("interbelic");
        c1.setCuvinteCheie(cheie);
        bc.adaugaCarte(c1);

    }

    @Test(expected =java.lang.Exception.class )
    public void adaugaCarteBVA_1_nonvalid() throws Exception {
        Carte c1= new Carte();
        c1.setTitlu("");
        List<String> referenti = new ArrayList<>();
        referenti.add("Ion Creanga");
        c1.setReferenti(referenti);
        c1.setAnAparitie(1899);
        List<String> cheie = new ArrayList<>();
        cheie.add("amintiri");
        cheie.add("copilarie");
        c1.setCuvinteCheie(cheie);
        bc.adaugaCarte(c1);

    }

    @Test
    public void adaugaCarteBVA_2_valid() throws Exception {
        Carte c1= new Carte();
        c1.setTitlu("I");
        List<String> referenti = new ArrayList<>();
        referenti.add("Ion Creanga");
        c1.setReferenti(referenti);
        c1.setAnAparitie(1);
        List<String> cheie = new ArrayList<>();
        cheie.add("amintiri");
        cheie.add("copilarie");
        c1.setCuvinteCheie(cheie);
        bc.adaugaCarte(c1);
        int car = bc.getCarti().size();
        assertTrue(car==7);
    }

    @Test(expected =java.lang.Exception.class )
    public void adaugaCarteBVA_3_nonvalid() throws Exception {
        Carte c1= new Carte();
        c1.setTitlu("Ion");
        List<String> referenti = new ArrayList<>();
        referenti.add("Liviu Rebreanu");
        c1.setReferenti(referenti);
        c1.setAnAparitie(-1);
        List<String> cheie = new ArrayList<>();
        cheie.add("roman");
        cheie.add("romanesc");
        c1.setCuvinteCheie(cheie);
        bc.adaugaCarte(c1);

    }

    @Test
    public void adaugaCarteBVA_4_valid() throws Exception {
        Carte c1= new Carte();
        c1.setTitlu("Io");
        List<String> referenti = new ArrayList<>();
        referenti.add("Ion Creanga");
        c1.setReferenti(referenti);
        c1.setAnAparitie(0);
        List<String> cheie = new ArrayList<>();
        cheie.add("amintiri");
        cheie.add("copilarie");
        c1.setCuvinteCheie(cheie);
        bc.adaugaCarte(c1);
        int car = bc.getCarti().size();
        assertTrue(car==7);
    }

    @Test
    public void cautaCarte_nonvalid(){

        try {
            bc.cautaCarte("12");
            assertTrue(false);
        }
        catch (Exception e){
            assertTrue(true);
        }
    }
}